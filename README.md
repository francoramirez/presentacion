A continuacion pasos a seguir para correr el jupyter correctamente:
IMPORTANTE: Correr comando en consola

    cd repositorio_local

1. Instalar maquina virtual

    python3.11 -m venv venv

2. Activar maquina

    source venv/Scripts/activate

3. Instalar requirementes.txt

    pip install -r requierementes.txt

4. Ejecutar jupyter

    jupyter notebook